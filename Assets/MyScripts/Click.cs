﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UIEventListener.Get (gameObject).onPress += _UIManager.Instance.Button_OnPress;
		UIEventListener.Get (gameObject).onDragEnd += _UIManager.Instance.Button_OnDragEnd;
	}
}
